import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../hero'

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HeroService } from '../hero.service'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {

  // @Input() hero: Hero;
  hero$: Observable<Hero> // implementation

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) { 
    // this.hero = Object();
  }

  ngOnInit(){
    this.getHero()
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    // this.heroService.getHero(id)
    //   .subscribe(hero => this.hero = hero);
    this.hero$ = this.heroService.getHero(id) // implementation of observable and async pipe
  }

  goBack(): void {
    this.location.back()
  }

  save(): void {
    this.heroService.updateHero(this.hero)
      .subscribe(()=> this.goBack())
  }
}
