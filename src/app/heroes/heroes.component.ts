import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit } from '@angular/core';

import { Hero } from '../hero'
import { HeroService } from '../hero.service'
import { MessageService } from '../message.service'
import { Observable } from 'rxjs'

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {

  // heroes: Hero[];

  heroes$: Observable<Hero[]>

  constructor(private heroService: HeroService, private messageService: MessageService) {
    
   }

  ngOnInit() {
    // this.getHeroes();
    this.heroes$ = this.heroService.getHeroes()
  }


  // getHeroes(): void{ //void means that there is nothing returned
  //   this.heroService.getHeroes() // calls the getHeroes function from hero.service 
  //     .subscribe(heroes => this.heroes = heroes) // once the heroes are loaded(response from above) and executes action within ()
  // }

  add(name: string): void {
    name = name.trim();
    if(!name) {return ;}
    this.heroService.addHero({ name } as Hero)
      .subscribe(hero => {
        this.heroes.push(hero)
      })
  }

  delete(hero: Hero): void {
    this.heroes$ = this.heroes.filter(h => h ! == hero);
    this.heroService.deleteHero(hero).subscribe();
  }

}
