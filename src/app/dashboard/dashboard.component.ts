import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs'

import { Hero } from '../hero'
import { HeroService } from '../hero.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  // heroes: Hero[]= [] // original code 
  heroes$: Observable<Hero[]>  // my implementation of Observable with async pipe

  constructor(private heroService: HeroService) {
    
   }

  ngOnInit() {
    // this.getHeroes() // original code 
  
    this.heroes$ = this.heroService.getHeroes() // my implementation
  }

  // getHeroes(): void {    //original code 
  //   this.heroService.getHeroes()
  //     .subscribe(heroes => this.heroes = heroes)
      
  //     // console.log(this.heroService.getHeroes().subscribe(heroes = > ))
  // }

}
